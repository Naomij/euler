# -*- coding: utf-8 -*-
"""
Created on Tue Feb 19 21:11:47 2019

@author: Naomi
"""
from decimal import *
import math as m
import sys
import scipy.stats as ss
import scipy.special as ssp
import numpy as np
import itertools

def isPrime(n):
    """"pre-condition: n is a nonnegative integer
    post-condition: return True if n is prime and False otherwise."""
    if n < 2: 
         return False
    if n % 2 == 0:             
         return n == 2  # return False
    k = 3
    while k*k <= n:
        if n % k == 0:
            return False
        k += 2
    return True

def phi(n):
    amount = 0        
    for k in range(1, n + 1):
        if m.gcd(n, k) == 1:
            amount += 1
    return amount

def primelist(n):
  """liste des n premier nombres premiers"""
  m = n+1
  #numbers = [True for i in range(m)]
  numbers = [True] * m #EDIT: faster
  for i in range(2, int(n**0.5 + 1)):
    if numbers[i]:
      for j in range(i*i, m, i):
        numbers[j] = False
  primes = []
  for i in range(2, m):
    if numbers[i]:
      primes.append(i)
  return primes

def convergent(lst):
    """donne la réduite de lst"""
    h0,h1=0,1
    k0,k1=1,0
    for a in lst:
        h1,h0=a*h1+h0,h1
        k1,k0=a*k1+k0,k1
    return (h1,k1)

def euler20(nombre=100): #fini
    n=1
    for i in range(1,nombre):
        n*=i
    s=0
    while n:
        s += n % 10
        n //= 10
    print(s)

def euler69(): #fini
   primes=[2,3,5,7,11,13,17,19,23,29,31]
   resultat=1
   for i in primes:
       if (i*resultat>=1000000):
           break
       else:
           resultat=resultat*i
   print(resultat)

                
def euler87(nombre=50_000_000): #fait
    i,j,k=0,0,0
    liste=[]
    racine2 = m.sqrt(nombre)+1
    premier = primelist(2*int(racine2))
    racine3 = nombre**0.34+1
    racine4 = m.sqrt(m.sqrt(nombre))+1
    count=0
    while premier[i]<=racine4:
        j=0
        while premier[j]<=racine3:
            k=0
            while premier[k]<=racine2:
                liste.append(premier[i]**4+premier[j]**3+premier[k]**2)
                count+=1
                k+=1
            j+=1
        i+=1
    finalement=set([i for i in liste if i<=nombre]) #éviter les doublons
    print(len(finalement))

def euler65(nombre=100): #fini
    e=[2]
    j=1
    for i in range(nombre-1):
        if (i%3==0 or i%3==2):
            e.append(1)
        else:
            e.append(2*j)
            j+=1
    (num,den)=convergent(e)
    result=0
    for n in str(num):
        result+=int(n)
    print(result)

def euler57(nombre=1000): #fini
    r2=[1]
    res=0
    for i in range(nombre):
        r2.append(2)
        num, den=(convergent(r2))
        num, den = len(str(num)),len(str(den))
        if num>den:
            res+=1
    print(res)

def euler13(): #fait 
    numbers=[37107287533902102798797998220837590246510135740250,46376937677490009712648124896970078050417018260538,
74324986199524741059474233309513058123726617309629,91942213363574161572522430563301811072406154908250,
23067588207539346171171980310421047513778063246676,89261670696623633820136378418383684178734361726757,
28112879812849979408065481931592621691275889832738,44274228917432520321923589422876796487670272189318,
47451445736001306439091167216856844588711603153276,70386486105843025439939619828917593665686757934951,
62176457141856560629502157223196586755079324193331,64906352462741904929101432445813822663347944758178,
92575867718337217661963751590579239728245598838407,58203565325359399008402633568948830189458628227828,
80181199384826282014278194139940567587151170094390,35398664372827112653829987240784473053190104293586,
86515506006295864861532075273371959191420517255829,71693888707715466499115593487603532921714970056938,
54370070576826684624621495650076471787294438377604,53282654108756828443191190634694037855217779295145,
36123272525000296071075082563815656710885258350721,45876576172410976447339110607218265236877223636045,
17423706905851860660448207621209813287860733969412,81142660418086830619328460811191061556940512689692,
51934325451728388641918047049293215058642563049483,62467221648435076201727918039944693004732956340691,
15732444386908125794514089057706229429197107928209,55037687525678773091862540744969844508330393682126,
18336384825330154686196124348767681297534375946515,80386287592878490201521685554828717201219257766954,
78182833757993103614740356856449095527097864797581,16726320100436897842553539920931837441497806860984,
48403098129077791799088218795327364475675590848030,87086987551392711854517078544161852424320693150332,
59959406895756536782107074926966537676326235447210,69793950679652694742597709739166693763042633987085,
41052684708299085211399427365734116182760315001271,65378607361501080857009149939512557028198746004375,
35829035317434717326932123578154982629742552737307,94953759765105305946966067683156574377167401875275,
88902802571733229619176668713819931811048770190271,25267680276078003013678680992525463401061632866526,
36270218540497705585629946580636237993140746255962,24074486908231174977792365466257246923322810917141,
91430288197103288597806669760892938638285025333403,34413065578016127815921815005561868836468420090470,
23053081172816430487623791969842487255036638784583,11487696932154902810424020138335124462181441773470,
63783299490636259666498587618221225225512486764533,67720186971698544312419572409913959008952310058822,
95548255300263520781532296796249481641953868218774,76085327132285723110424803456124867697064507995236,
37774242535411291684276865538926205024910326572967,23701913275725675285653248258265463092207058596522,
29798860272258331913126375147341994889534765745501,18495701454879288984856827726077713721403798879715,
38298203783031473527721580348144513491373226651381,34829543829199918180278916522431027392251122869539,
40957953066405232632538044100059654939159879593635,29746152185502371307642255121183693803580388584903,
41698116222072977186158236678424689157993532961922,62467957194401269043877107275048102390895523597457,
23189706772547915061505504953922979530901129967519,86188088225875314529584099251203829009407770775672,
11306739708304724483816533873502340845647058077308,82959174767140363198008187129011875491310547126581,
97623331044818386269515456334926366572897563400500,42846280183517070527831839425882145521227251250327,
55121603546981200581762165212827652751691296897789,32238195734329339946437501907836945765883352399886,
75506164965184775180738168837861091527357929701337,62177842752192623401942399639168044983993173312731,
32924185707147349566916674687634660915035914677504,99518671430235219628894890102423325116913619626622,
73267460800591547471830798392868535206946944540724,76841822524674417161514036427982273348055556214818,
97142617910342598647204516893989422179826088076852,87783646182799346313767754307809363333018982642090,
10848802521674670883215120185883543223812876952786,71329612474782464538636993009049310363619763878039,
62184073572399794223406235393808339651327408011116,66627891981488087797941876876144230030984490851411,
60661826293682836764744779239180335110989069790714,85786944089552990653640447425576083659976645795096,
66024396409905389607120198219976047599490197230297,64913982680032973156037120041377903785566085089252,
16730939319872750275468906903707539413042652315011,94809377245048795150954100921645863754710598436791,
78639167021187492431995700641917969777599028300699,15368713711936614952811305876380278410754449733078,
40789923115535562561142322423255033685442488917353,44889911501440648020369068063960672322193204149535,
41503128880339536053299340368006977710650566631954,81234880673210146739058568557934581403627822703280,
82616570773948327592232845941706525094512325230608,22918802058777319719839450180888072429661980811197,
77158542502016545090413245809786882778948721859617,72107838435069186155435662884062257473692284509516,
20849603980134001723930671666823555245252804609722,53503534226472524250874054075591789781264330331690]
    result=0
    for i in numbers:
        result+=i
    print(str(result)[0:10])


def euler72(n=1_000_000): #fait
    s = 0
    n
    n+=1
    phi=np.zeros(n)
    phi[1] = 1
    i = 2
    while i < n:
        if phi[i] == 0:
            phi[i] = i - 1

            j = 2

            while j * i < n:
                if phi[j] != 0:
                    q = j
                    f = i - 1

                    while q % i == 0:
                        f *= i
                        q //= i

                    phi[i * j] = f * phi[q]
                j += 1
        s += phi[i]
        i += 1
    print(int(s))

def euler58(n=4): #horriblement inefficace, pas encore fini
    nombres = list(range(1,n))
    ur= [isPrime(4*i**2-2*i+1) for i in nombres]
    ul= [isPrime(4*i**2+1) for i in nombres]
    bl= [isPrime(4*i**2+2*i+1) for i in nombres]
    br= [isPrime(4*i**2+4*i+1) for i in nombres]
    for i in range(2,n):
        if (np.sum(ur[0:i])+np.sum(ul[0:i])+np.sum(bl[0:i])+np.sum(br[0:i]))/(4*i)<=0.1 :
            break
    print((np.sum(ur[0:i])+np.sum(ul[0:i])+np.sum(bl[0:i])+np.sum(br[0:i]))/(4*i))
    print(i)

def euler8(): #fait
    res=0
    prod="7316717653133062491922511967442657474235534919493496983520312774506326239578318016984801869478851843\
8586156078911294949545950173795833195285320880551112540698747158523863050715693290963295227443043557\
6689664895044524452316173185640309871112172238311362229893423380308135336276614282806444486645238749\
3035890729629049156044077239071381051585930796086670172427121883998797908792274921901699720888093776\
6572733300105336788122023542180975125454059475224352584907711670556013604839586446706324415722155397\
5369781797784617406495514929086256932197846862248283972241375657056057490261407972968652414535100474\
8216637048440319989000889524345065854122758866688116427171479924442928230863465674813919123162824586\
1786645835912456652947654568284891288314260769004224219022671055626321111109370544217506941658960408\
0719840385096245544436298123098787992724428490918884580156166097919133875499200524063689912560717606\
0588611646710940507754100225698315520005593572972571636269561882670428252483600823257530420752963450"
    for i in range(len(prod)-11):
        buf=1
        for j in prod[i:i+13]:
            buf*=int(j)
        if buf>res:
            res=buf
    print(res)

def euler97(): #fait
    print(str(28433*pow(2,7830457,10000000000)+1))

def euler104(n=27500):
    a,b = (1,1)
    for i in range(2,n+1):
        (a,b)=(b,a+b)
        if(sorted(str(a)[0:9])==['1', '2', '3', '4', '5', '6', '7', '8', '9']):
            if (sorted(str(a)[-9:])==['1', '2', '3', '4', '5', '6', '7', '8', '9']):
                print("i")


def euler48(): #fait
    somme=0
    mod=10**10
    for i in range(1,1001):
        somme += pow(i,i,mod)
    print(somme%mod)


def rpn(lst):
    stack=[]
    for i in lst:
        if str(i).isnumeric():
            stack.append(i)
        try:
            if i=='+':
                stack.append(stack.pop()+stack.pop())
            if i=='*':
                stack.append(stack.pop()*stack.pop())
            if i=='-':
                stack.append((stack.pop()-stack.pop()))
            if i=='/':
                (a,b)=(stack.pop(),stack.pop())
                if b==0:
                    stack.append(0)
                elif a%b==0:
                    stack.append(int(a/b))
                else :
                    stack.append(a/b)
        except:
            stack=[0]
    if float(stack[0]).is_integer():
        return int(stack[0])
    else:
        return 0
def baseN(num,b,numerals="012345678ba"):
    a = ((num == 0) and numerals[0]) or (baseN(num // b, b, numerals).lstrip(numerals[0]) + numerals[num % b])
    if len(a)==3:
        return a
    if len(a)==2:
        return '0'+a
    if len(a)==1:
        return '00'+a

def baseN2(num,b,numerals="012345ba"):
    a = ((num == 0) and numerals[0]) or (baseN(num // b, b, numerals).lstrip(numerals[0]) + numerals[num % b])
    if len(a)==2:
        return a
    if len(a)==1:
        return '0'+a


def euler93(rangemax=40): #fait
    k=0
    sett=set()
    dic=np.array(['+','-','*','/'])
    operations=['*','+','-']
    for a in range(0,10):
        for b in range(a+1,10):
            for c in range(b+1,10):
                for d in range(c+1,10):
                    four=[a,b,c,d]
                    for i in itertools.permutations(four):
                        for j in range(0,3):
                            operations=[dic[j]]
                            r=rpn(list(i[0:2])+list(operations))
                            if r>0:
                                sett.add(r)
                        for j in range(0,16):
                            operations=dic[list(map(int,list(baseN2(j,4))))]
                            r=rpn(list(i[0:3])+list(operations))
                            if r>0:
                                sett.add(r)
                        for j in range(0,64):
                            operations=dic[list(map(int,list(baseN(j,4))))]
                            r=rpn(list(i)+list(operations))
                            sett.add(r)
                            r=rpn(list(i)[0:3]+[list(operations)[0]]+[list(i)[3]]+list(operations[1:4]))
                            sett.add(r)
                            r=rpn(list(i)[0:2]+[list(operations)[0]]+list(i)[2:4]+list(operations[1:4]))
                            sett.add(r)
                            r=rpn(list(i)[0:2]+[list(operations)[0]]+[list(i)[2]]+[list(operations[1])]+[list(i)[3]]+[list(operations[2])])
                            sett.add(r)
                            r=rpn(list(i)[0:2]+[list(operations)[0]]+[list(i)[2]]+[list(operations[1])]+[list(i)[3]]+[list(operations[2])])
                            sett.add(r)
                    sett=sett.union(set(four))
                    if set(range(1,rangemax)).issubset(sett):
                        print(four,sett)
                        k+=1
                    sett=set()
    print(k)

def numberToBase(n, b):
    if n == 0:
        return [0]
    digits = []
    while n:
        digits.append(int(n % b))
        n //= b
    return digits[::-1]
def euler932(rangemax=40):
    k=0
    sett=set()
    dic=np.array(['+','-','*','/'])
    dico=dict()
    for l in range(62748517): # 62748517 
        if np.sum(np.append(np.zeros(13-len(numberToBase(l,13)),dtype=int),numberToBase(l,13))<9)==4:
            liste=list(np.zeros(13-len(numberToBase(l,13)),dtype=int))+numberToBase(l,13)
            nombres=[l+1 for l in liste if l<9 ]
            nombres=sorted(nombres)
            print(nombres)
            liste=[l+1 if l <9 else dic[l-9] for l in liste]
            if (''.join(map(str,nombres)) in dico):
                dico[''.join(map(str,nombres))].add(rpn(liste))
            else:
                dico[''.join(map(str,nombres))]={rpn(liste)}
        if l%60000==0:
            print(l)
    print(dico)

def sqrtcf(n):
    """ n n'est pas un carré"""
    getcontext().prec = 100
    lst=[]
    lst.append(Decimal(int(m.sqrt(n))))
    b1=Decimal(1)/(Decimal(m.sqrt(n))-lst[0])
    bi=b1
    lst.append(Decimal(int(bi)))
    for i in range(1,100):
        bi=Decimal(1)/(bi-lst[i])
        print(bi)
        lst.append(Decimal(int(bi)))
        if(abs(b1-bi)<0.001) and lst[i]==2*lst[0]:
            lst.pop()
            break
#        if(lst[i+1]==2*lst[0]):
 #           break
    return lst

def euler66(n):
    f=open("b")
    f.readlines(10)
    total=[]
    for i in range(n):
        if not m.sqrt(i).is_integer():
            lst=sqrtcf(i)
            if len(lst)<200:
                if i%2==0:
                    total+=[i,convergent(lst[0:len(lst)-1])[0]]
                else:
                    p=len(lst)
                    lst=lst+lst[1:]
                    total+=[i,convergent(lst[0:len(lst)-1])[0]]
    return total

def euler53(): #fait
    summ=0
    p=[[0,1,1,0]]
    for j in range(99):
        p.append(([0]+[p[j][i]+p[j][i+1] for i in range(len(p[j])-1)]+[0]))
    for i in p:
        for j in i:
            if j>1000000:
                summ+=1
    print(summ)
    
def euler80(): #fait
    s=0
    getcontext().prec = 150
    for n in range(1,100):
        if not m.sqrt(n).is_integer():
            a=Decimal(m.sqrt(n))
            for i in range(100):
                if a-Decimal(a**2-n)/Decimal(2*a)==a:
                    break
                a=Decimal(a-Decimal(a**2-n)/Decimal(2*a))
            s+=(np.array(list(str(a))[2:101], dtype=int).sum())+int(m.sqrt(n))
    print(s)

def naiveeuler76(n,lst):
    for i in range(1,n):
        if(i+lst.sum()==n):
            print( np.append(lst,i))
        if(i+lst.sum()<n):
            return naiveeuler76(n,np.append(lst,i))
        if (i+lst.sum()>n):
            np.delete(lst, 1, 0)

def euler36(n):
    a=0
    for i in range(n):
        if str(i)==str(i)[::-1] and bin(i)[2:]==bin(i)[2:][::-1]:
            a+=i
    return a

def euler26(): #fait, eh
    return primelist(1000)

def euler29(): #fait
    lst=[]
    for a in range(2,101):
        for b in range(2,101):
            lst.append(a**b)
    return len(set(lst))

def euler44():
    pen = lambda x : ((m.sqrt(24*x+1)+1)/6).is_integer()
    p=[int(n*(3*n-1)/2) for n in range(1,3000)]
    for a in p:
        for b in p:
            if pen(a+b) and pen(abs(a-b)):
                print(a-b)
                
def euler45():
    pen = lambda x : ((m.sqrt(24*x+1)+1)/6).is_integer()
    tri = lambda x : ((m.sqrt(8*x+1)-1)/2).is_integer()
    for n in range(30000):
        if pen(n*(2*n-1) ) and tri(n*(2*n-1) ):
            print(n*(2*n-1))
            
def euler27():
    p=primelist(1000)
    print(p)
    for b in p:
        for a in range(-1000,1000):
            pass
            
def euler50(): #fait
    p=primelist(4000)
    n=len(p)
    for k in range(700,50,-1):
        for i in range(0,n-k):
            if np.sum(p[i:i+k])<1000000 and isPrime(np.sum(p[i:i+k])):
                print(k,p[i:i+k],np.sum(p[i:i+k]))
                return

def euler15(n=20):
    """Il faut simplement calculer les manières d'arranger n chemins vers le bas et n chemins vers le haut
    soit donc C_n^2n"""
    print(ssp.comb(2*n,n,exact=True))


def euler99():
    """Il suffit d'utiliser le fait que $\log(a^b)=b\log(a)$ et de comparer les logs"""
    file = open("p099_base_exp.txt")
    Lines = file.readlines()
    maxi,i,n=0,0,0
    for line in Lines:
        i+=1
        line2 = line.split(",")
        res = float(line2[1])*m.log(float(line2[0]))
        if res > maxi:
            maxi = res
            n=i
    print(n)



if __name__ == "__main__":
    if (len(sys.argv)==2):
        arg = sys.argv[1]
        try:
            func=globals()["euler"+arg]
            func()          
        except:
            print("Wrong number")
        
