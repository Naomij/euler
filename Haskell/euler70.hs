
import qualified Data.Set as PQ
import qualified Data.List as DL

main :: IO ()
main = do
    let a = f (reverse $  take 500 primes) (reverse $ take 500 primes)
    print $ head(a !! 2)  -- faire ça bien

g :: Integer -> [Integer] -> [Integer]
g _ [] = []
g a (x:xs) = if DL.sort (digs (a*x)) == DL.sort (digs ((a-1)*(x-1))) then a*x:g a xs else g a xs


-- Recursion proceeds by analysing each element of the first list
-- and multiply it by all elements of the second
f :: [Integer] -> [Integer] -> [[Integer]]
f [] _ = [[]]
f (x:xs) b = g x b : f xs b

-- prendre deux nombres premiers jusqu'à 3162 et les multiplier pour minimiser phi(n)

digs :: Integral x => x -> [x]
digs 0 = []
digs x = x `mod` 10 : digs (x `div` 10)

primes :: [Integer]
primes = 2:sieve [3,5..]
  where
    sieve (x:xs) = x : sieve' xs (insertprime x xs PQ.empty)

    sieve' (x:xs) table
        | nextComposite == x = sieve' xs (adjust x table)
        | otherwise          = x : sieve' xs (insertprime x xs table)
      where
        (nextComposite,_) = PQ.findMin table

    adjust x table
        | n == x    = adjust x (PQ.insert (n', ns) newPQ)
        | otherwise = table
      where
        Just ((n, n':ns), newPQ) = PQ.minView table

    insertprime p xs = PQ.insert (p*p, map (*p) xs)
