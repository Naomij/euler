import Data.List

lognum (b,e) = e * log b
logfun x = lognum . read $ "(" ++ x ++ ")"
problem_99 = snd . maximum . flip zip [1..] . map logfun . lines
main = readFile "p099_base_exp.txt" >>= print . problem_99
