# Project Euler

This is a repository of some of my personal solutions in multiple languages to [Project Euler](https://projecteuler.net/about)

The APL code is written in Dyalog APL and can be executed using tryapl